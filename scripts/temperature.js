// Temperature class with conversion methods
class Temperature {
    constructor(temp_from_value, temp_to_suffix) {
        this.temp_from_value = temp_from_value;
        this.temp_to_suffix = temp_to_suffix;
    }

    fahrenheitToCelsius() {
        let result = (this.temp_from_value - 32) / 9;
        return `${result} ${this.temp_to_suffix}`
    }

    fahrenheitToKelvin() {
        let result = (this.temp_from_value + 459.67) * (5 / 9);
        return `${result} ${this.temp_to_suffix}`
    }

    fahrenheitToRankine() {
        let result = this.temp_from_value + 459.67;
        return `${result} ${this.temp_to_suffix}`
    }

    kelvinToCelsius() {
        let result = this.temp_from_value - 237.15;
        return `${result} ${this.temp_to_suffix}`
    }

    kelvinToFahrenheit() {
        let result = this.temp_from_value * (9 / 5) - 459.67;
        return `${result} ${this.temp_to_suffix}`
    }

    kelvinToRankine() {
        let result = this.temp_from_value * (9 / 5);
        return `${result} ${this.temp_to_suffix}`
    }

    celsiusToKelvin() {
        let result = this.temp_from_value + 273.15;
        return `${result} ${this.temp_to_suffix}`
    }

    celsiusToRankine() {
        let result = (this.temp_from_value + 273.15) * (9 / 5);
        return `${result} ${this.temp_to_suffix}`
    }

    celsiusToFahrenheit() {
        let result = (this.temp_from_value * 9 / 5) + 32;
        return `${result} ${this.temp_to_suffix}`
    }

    rankineToFahrenheit() {
        let result = this.temp_from_value - 459.67;
        return `${result} ${this.temp_to_suffix}`
    }

    rankineToCelsius() {
        let result = (this.temp_from_value - 459.67) * (5 / 9);
        return `${result} ${this.temp_to_suffix}`
    }

    rankineToKelvin() {
        let result = this.temp_from_value * (5 / 9);
        return `${result} ${this.temp_to_suffix}`
    }
}

let initialValue = document.getElementById('temp-value');
const unitsOptions = document.getElementById('temperature');
const calculate = document.getElementById('calculate');
const result = document.getElementById('result');

unitsOptions.addEventListener('change', ($event) => {
    switch($event.target.value) {
        case "Fahrenheit to Celsius":
            calculate.addEventListener('click', (e) => {
                let fc = new Temperature(initialValue.value, 'degree Celsius');
                e.preventDefault();
                result.innerHTML = fC.fahrenheitToCelsius();
            });
            break;
        case "Fahrenheit to Kelvin":
            calculate.addEventListener('click', (e) => {
                let fK = new Temperature(initialValue.value, 'Kelvin');
                e.preventDefault();
                result.innerHTML = fK.fahrenheitToKelvin();
            })
            break;
        case "Fahrenheit to Rankine":
            calculate.addEventListener('click', (e) => {
                let fR = new Temperature(initialValue.value, 'Rankine');
                e.preventDefault();
                result.innerHTML = fR.fahrenheitToRankine();
            })
            break;
        case "Kelvin to Celsius":
            calculate.addEventListener('click', (e) => {
                let kC = new Temperature(initialValue.value, 'degree Celsius');
                e.preventDefault();
                result.innerHTML = kC.kelvinToCelsius();
            })
            break;
        case "Kelvin to Fahrenheit":
            calculate.addEventListener('click', (e) => {
                let kF = new Temperature(initialValue.value, 'degree Fahrenheit');
                e.preventDefault();
                result.innerHTML = kF.kelvinToFahrenheit();
            })
            break
        case "Kelvin to Rankine":
            calculate.addEventListener('click', (e) => {
                let kR = new Temperature(initialValue.value, 'Rankine');
                e.preventDefault();
                result.innerHTML = kR.kelvinToRankine();
            })
            break
        case "Celsius to Kelvin":
            calculate.addEventListener('click', (e) => {
                let cK = new Temperature(initialValue.value, 'Kelvin');
                e.preventDefault();
                result.innerHTML = cK.celsiusToKelvin();
            })
            break
        case "Celsius to Rankine":
            calculate.addEventListener('click', (e) => {
                let cR = new Temperature(initialValue.value, 'Rankine');
                e.preventDefault();
                result.innerHTML = cR.celsiusToRankine();
            })
            break
        case "Celsius to Fahrenheit":
            calculate.addEventListener('click', (e) => {
                let cF = new Temperature(initialValue.value, 'degree Fahrenheit');
                e.preventDefault();
                result.innerHTML = cF.celsiusToFahrenheit();
            })
            break
        case "Rankine to Fahrenheit":
            calculate.addEventListener('click', (e) => {
                let rF = new Temperature(initialValue.value, 'degree Fahrenheit');
                e.preventDefault();
                result.innerHTML = rF.rankineToFahrenheit();
            })
            break
        case "Rankine to Celsius":
            calculate.addEventListener('click', (e) => {
                let rC = new Temperature(initialValue.value, 'degree Celsius');
                e.preventDefault();
                result.innerHTML = rC.rankineToCelsius();
            })
            break
        case "Rankine to Kelvin":
            calculate.addEventListener('click', (e) => {
                let rK = new Temperature(initialValue.value, 'Kelvin');
                e.preventDefault();
                result.innerHTML = rK.rankineToKelvin();
            })
            break
        default:
            calculate.addEventListener('click', (e) => {
                let errorMessage = "You have to select a pair of conversion units";
                e.preventDefault();
                result.innerHTML = errorMessage;
            })
    } 
});