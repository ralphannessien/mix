var contactBase = [];

function addContact(myName){
    if(!fieldEmpty()){ 
        myName =   document.getElementById("name").value;
        var myAddress =   document.getElementById("address").value;
        var myEmail = document.getElementById("email").value;
        var myPhone = document.getElementById("phone").value;
        
        var position = document.getElementById("contactList");
        var newElement = document.createElement('li');
        var newText = document.createTextNode(myName);
        newElement.appendChild(newText);
        position.appendChild(newElement);
        // clear all fields
        document.getElementById("name").value = "";
        document.getElementById("address").value = "";
        document.getElementById("email").value = "";
        document.getElementById("phone").value = "";
    
        var contactObject = {name:myName, address:myAddress, email:myEmail, phone:myPhone}
        contactBase.push(contactObject);
        
        var specialLink = document.querySelector('ul');
        specialLink.addEventListener("click", function(e){
            for(var i = 0; i < contactBase.length; i++){
                if(e.target.tagName == "LI" && e.target.innerHTML == contactBase[i].name){
                    document.getElementById("showtext").value = 'Address: ' + contactBase[i].address + '\n' + 'Email: ' + contactBase[i].email + '\n' + 'Phone: ' + contactBase[i].phone;
                }
            }
        }, false);
    }
}
  
function fieldEmpty(){
    var isEmpty = false,
        myname = document.getElementById("name").value,
        myaddress = document.getElementById("address").value,
        myemail = document.getElementById("email").value;
        myphone = document.getElementById("phone").value;
  
    if(myname === "" || myaddress === "" || myemail === "" || myphone === ""){
        alert("operation is not allowed!");
        isEmpty = true;
    }

    return isEmpty;
}
  
function clearScreen(){
    document.getElementById("showtext").value = "";
}