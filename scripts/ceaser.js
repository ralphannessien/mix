document.getElementById('encrypt').onclick = function() {cipher(document.getElementById('usertext').value)}

function cipher(text) {
	const shift = 5;
    let cipherString = "";
    // Loop over each character in the string
    for(let item = 0; item < text.length; item++){
        let single = text[item];

        if(!single.match(/[A-Za-z]/i)){
            alert('None string characters are not required!: INVALID USER INPUT');
            return false;
        }
        else {
            let digit = text.charCodeAt(item);
            
            //encrypt uppercase letters
            if((digit >= 65) && (digit <= 90))
                single = String.fromCharCode(((digit + 65 - shift) % 26) + 65);
            
            //encrypt early lowercase letters
            if((digit >= 97) && (digit <= 101))
                single = String.fromCharCode(((digit + 97 - shift) % 97) + 26);
            
            //encrypt subsequent lowercase letters
            if((digit >= 102) && (digit <= 122))
                single = String.fromCharCode(((digit + 97 - shift) % 97) + 97);
        }
        cipherString += single;
    }
    document.getElementById('ciphercode').textContent = cipherString;
    document.getElementById('usertext').value = "";
}




document.getElementById('decrypt').onclick = function() {decipher(document.getElementById('usertext').value)}

function decipher(ciphertext) {
	const pushops = 5;
    let decipherString = "";
    // Loop over each character in the string
    for(let item = 0; item < ciphertext.length; item++){
        let oneChar = ciphertext[item];

        if(!oneChar.match(/[A-Za-z]/i)){
            alert('None string characters are not required!: INVALID USER INPUT');
            return false;
        }
        else {
            let digit = ciphertext.charCodeAt(item);
            
            //encrypt uppercase letters
            if((digit >= 65) && (digit <= 90))
                oneChar = String.fromCharCode(((digit + 65 + pushops) % 26) + 65);
            
            //decrypt lowercase letters
            if((digit >= 97) && (digit <= 122))
                oneChar = String.fromCharCode(((digit - 97 + pushops) % 26) + 97);
        }
        decipherString += oneChar;
    }
    document.getElementById('ciphercode').textContent = decipherString;
    document.getElementById('usertext').value = "";
}

document.getElementById('clear').onclick = clearcrypt;

function clearcrypt(){
    return document.getElementById('ciphercode').textContent = "";
}

document.getElementById('ciphercode').ondblclick = reInput;

function reInput(){
    let a = document.getElementById('ciphercode').innerHTML;
    document.getElementById('usertext').value = a;
    document.getElementById('ciphercode').textContent = "";
}