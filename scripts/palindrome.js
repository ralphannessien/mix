var entry = document.getElementById("wordOrPhrase").value
var entryCache = [];

document.getElementById('check').onclick = function() {testEntry(document.getElementById("wordOrPhrase").value)}
function testEntry(myStr) {
    var myNuStr = (myStr.split(" ").join(''));
    myNuStr = myNuStr.toLowerCase();
    var strRev = myNuStr.split('').reverse().join('');
    if (myNuStr == ""){
        document.getElementById("text").innerHTML = "Provide a word or a phrase!!!"
    }
    else if(strRev === myNuStr){
        //alert(myStr + " is a Palindrome");
        document.getElementById("text").innerHTML = (`${myStr} is a Palindrome`);
    }
    else {
        //alert(myStr + "is not a Palindrome");
        document.getElementById("text").innerHTML = (`${myStr} is not a Palindrome`)
    }
  save();
}

function save(){
    var val = document.getElementById("wordOrPhrase").value;
    if(val != ""){
        entryCache.push(val);
    }
    if(entryCache.length == 6){ 
        entryCache.shift();
    }
    if(entryCache[0]){
        document.getElementById("zero").innerHTML = entryCache[0];
    }
    if(entryCache[1]){
        document.getElementById("one").innerHTML = entryCache[1];
    }
    if(entryCache[2]){
        document.getElementById("two").innerHTML = entryCache[2];
    }
    if(entryCache[3]){
       document.getElementById("three").innerHTML = entryCache[3];
    }
    if(entryCache[4]){
        document.getElementById("four").innerHTML = entryCache[4];
    }
  on()
}

function on() {
    document.getElementById("overlay").style.display = "block";
}
    
function off() {
    document.getElementById("overlay").style.display = "none";
    document.getElementById("wordOrPhrase").value = "";
}
