document.addEventListener('DOMContentLoaded', () => {
    let mySidebar = document.querySelector('#collapsible-sidebar');
    let landing = document.querySelector('#landing');
    let toggler = document.querySelector('#togglesidebar');
    
    toggler.addEventListener('click', function () {
        mySidebar.classList.toggle("display");
        landing.classList.toggle("heromain");
    });
});