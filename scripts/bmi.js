class Calculator {
    constructor(weight, height, name) {
        this.weight = weight;
        this.height = height;
        this.name = name;
    }
    poundsInches() {
        let bmi1 = (this.weight * 703) / (this.height ** 2)
        return `Hello ${this.name}, this is the American metrics and your Body Mass Index is ${bmi1}.`;
    }

    KilogramsCm() {
        let bmi2 = this.weight / (this.height ** 2)
        return `Hello ${this.name}, this is the British metrics and your Body Mass Index is ${bmi2}`;
    }
}

// Form elements
let getMetrics = document.getElementsByName('metrics');
let calculate = document.getElementById('calculate');
let result = document.getElementById('result');

let givenHeight = document.getElementById('weight');
let givenWeight = document.getElementById('weight');
let givenName = document.getElementById('username');



for (let i = 0; i < getMetrics.length; i++) {
    getMetrics[i].addEventListener('change', ($event) => {
        if ($event.target.value === "lbs-inch") {
            calculate.addEventListener('click', ($event) => {
                let americanMetrics = new Calculator(givenWeight.value, givenHeight.value, givenName.value);
                $event.preventDefault()
                result.innerHTML = americanMetrics.poundsInches();
                document.getElementById("myForm").reset();
            })
        }
        else if ($event.target.value === "kg-cm") {
            calculate.addEventListener('click', ($event) => {
                let britishMetrics = new Calculator(givenWeight.value, givenHeight.value, givenName.value);
                $event.preventDefault()
                result.innerHTML = britishMetrics.KilogramsCm();
                document.getElementById("myForm").reset();
            })
        }
    });
}
