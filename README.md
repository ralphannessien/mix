# A Mix of Things

"**A Mix of Things**" is simply a handful of simple applications created with JavaScript, CSS, and HTML. The whole idea is to gather some of the easiest applications together so that newbies to programming can find ease of learning.

This side-menu user dashboard pattern was adopted for two reasons. The first reason is to allow for easy additions of more simple applications to the website, while the second reason is to showcase how easy it is to set up a simple dashboard. By going through this code, you will have no problem setting up your own.

I really hope learners have a good time learning a few things from the app as much as I did putting it all together.

